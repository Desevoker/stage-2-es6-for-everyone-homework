import ControlsView from './controlsView';
import FightersView from './fightersView';
import { fighterService } from './services/fightersService';

class App {
  constructor() {
    this.startApp();
  }

  rootElement = document.getElementById('root');

  loadingElement = document.getElementById('loading-overlay');

  async startApp() {
    try {
      this.loadingElement.style.visibility = 'visible';

      const fighters = await fighterService.getFighters();
      const fightersView = new FightersView(fighters);
      const fightersElement = fightersView.element;
      const controlsView = new ControlsView(fighters, fightersView);
      const controlsElement = controlsView.element;

      this.rootElement.appendChild(fightersElement);
      this.rootElement.appendChild(controlsElement);
    } catch (error) {
      console.warn(error);
      this.rootElement.innerText = 'Failed to load data';
    } finally {
      this.loadingElement.style.visibility = 'hidden';
    }
  }
}

export default App;
