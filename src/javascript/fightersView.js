import * as Tingle from 'tingle.js';
import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';

class FightersView extends View {
  constructor(fighters) {
    super();

    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);

    this.modal = new Tingle.modal({
      footer: true,
      stickyFooter: false,
      closeMethods: ['overlay', 'button', 'escape'],
      closeLabel: 'Close',
    });

    this.modal.addFooterBtn(
      'Reset',
      'tingle-btn tingle-btn--danger',
      async () => {
        try {
          const _id = document.getElementById('_id').value;
          const healthInput = document.getElementById('health');
          const attackInput = document.getElementById('attack');
          const defenseInput = document.getElementById('defense');

          const fighterDetails = await this.resetFighterDetails(_id);

          healthInput.value = fighterDetails.health;
          attackInput.value = fighterDetails.attack;
          defenseInput.value = fighterDetails.defense;
        } catch (error) {
          console.warn(error);
        }
      }
    );

    this.modal.addFooterBtn(
      'Save',
      'tingle-btn tingle-btn--primary',
      () => {
        try {
          const _id = document.getElementById('_id').value;
          const healthInput = document.getElementById('health');
          const health = document.getElementById('health').value;
          const attackInput = document.getElementById('attack');
          const attack = document.getElementById('attack').value;
          const defenseInput = document.getElementById('defense');
          const defense = document.getElementById('defense').value;

          const fighterDetails = this.fightersDetailsMap.get(_id);

          if (health > 0 && health <= 1000) {
            fighterDetails.health = health;
          } else {
            healthInput.value = fighterDetails.health;
          }

          if (attack > 0 && attack <= 100) {
            fighterDetails.attack = attack;
          } else {
            attackInput.value = fighterDetails.attack;
          }

          if (defense > 0 && defense <= 100) {
            fighterDetails.defense = defense;
          } else {
            defenseInput.value = fighterDetails.defense;
          }
        } catch (error) {
          console.warn(error);
        }
      }
    );

    this.modal.addFooterBtn(
      'Close',
      'tingle-btn tingle-btn--default tingle-btn--pull-right',
      () => { this.modal.close(); }
    );
  }

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({
      tagName: 'div',
      className: 'fighters',
    });
    this.element.append(...fighterElements);
  }

  async handleFighterClick(fighter) {
    try {
      const fighterDetails = await this.getFighterDetails(fighter._id);

      if (typeof fighterDetails === 'object') {
        const { _id, name, source, health, attack, defense } = fighterDetails;

        this.modal.setContent(
          `<h2>${name}</h2>
          <div><img class="fighter-image" src="${source}"></div>
          <input id="_id" type="hidden" value="${_id}">
          <div><label>Health: <input id="health" type="number" min="1" value="${health}"></label></div>
          <div><label>Attack: <input id="attack" type="number" min="1" value="${attack}"></label></div>
          <div><label>Defense: <input id="defense" type="number" min="1" value="${defense}"></label></div>`
        );
        this.modal.open();
      } else {
        this.modal.setContent('Failed to load data');
        this.modal.open();
      }
    } catch (error) {
      console.warn(error);
    }
  }

  async getFighterDetails(_id) {
    try {
      if (!this.fightersDetailsMap.has(_id)) {
        const fighterDetails = await fighterService.getFighterDetails(_id);
        this.fightersDetailsMap.set(_id, fighterDetails);
      }

      return this.fightersDetailsMap.get(_id);
    } catch (error) {
      console.warn(error);
    }
  }

  async resetFighterDetails(_id) {
    try {
      if (this.fightersDetailsMap.delete(_id)) {
        return await this.getFighterDetails(_id);
      }
    } catch (error) {
      console.warn(error);
    }
  }
}

export default FightersView;
