import * as Tingle from 'tingle.js';
import View from './view';
import Fighter from './fighter';

class ControlsView extends View {
  constructor(fighters, fightersView) {
    super();

    this.fightersCount = fighters.length;
    this.fightersView = fightersView;
    this.createControls(fighters);

    this.modal = new Tingle.modal({
      footer: true,
      stickyFooter: false,
      closeMethods: ['overlay', 'button', 'escape'],
      closeLabel: 'Close',
    });

    this.modal.addFooterBtn(
      'Revenge!',
      'tingle-btn tingle-btn--danger',
      async () => {
        const _id1 = document.getElementById('_id1').value;
        const _id2 = document.getElementById('_id2').value;
        this.prepareAndFight(_id1, _id2);
      }
    );

    this.modal.addFooterBtn(
      'Close',
      'tingle-btn tingle-btn--default tingle-btn--pull-right',
      () => { this.modal.close(); }
    );
  }

  createControls(fighters) {
    const fightersOptions = fighters.map(fighter => {
      const option = this.createElement({
        tagName: 'option',
        className: '',
        attributes: { value: `${fighter._id}` },
      });
      option.innerText = `${fighter.name}`;
      return option;
    });

    const selectFirst = this.createElement({
      tagName: 'select',
      className: '',
      attributes: { id: 'first', name: 'first' },
    });
    const selectSecond = this.createElement({
      tagName: 'select',
      className: '',
      attributes: { id: 'second', name: 'second' },
    });

    fightersOptions.forEach(option => {
      const optionClone = new Option(option.text, option.value);
      selectFirst.add(option);
      selectSecond.add(optionClone);
    });

    const span = this.createElement({ tagName: 'span', className: 'vs' });
    span.innerText = 'VS';

    const button = this.createElement({
      tagName: 'button',
      className: 'button-fight',
      attributes: { type: 'button' },
    });
    button.innerText = 'FIGHT';
    button.classList.add('tingle-btn');
    button.classList.add('tingle-btn--danger');
    button.addEventListener('click', () => { this.prepareAndFight(selectFirst.value, selectSecond.value); }, false);

    this.element = this.createElement({
      tagName: 'div',
      className: 'controls',
    });
    this.element.append(selectFirst, span, selectSecond, button);
  }

  async prepareAndFight(firstId, secondId) {
    if (
      (firstId >= 1 && firstId <= this.fightersCount) &&
      (secondId >= 1 && secondId <= this.fightersCount)
    ) {
      try {
        const firstFighterDetails = await this.fightersView.getFighterDetails(firstId);
        const firstFighter = new Fighter(firstFighterDetails);
        const secondFighterDetails = await this.fightersView.getFighterDetails(secondId);
        const secondFighter = new Fighter(secondFighterDetails);

        this.fight(firstFighter, secondFighter);
      } catch (error) {
        console.warn(error);
      }
    }
  }

  fight(firstFighter, secondFighter) {
    while (firstFighter.health > 0 && secondFighter.health > 0) {
      const firstFighterPunch = firstFighter.getHitPower() - secondFighter.getBlockPower();
      const secondFighterPunch = secondFighter.getHitPower() - firstFighter.getBlockPower();

      if (firstFighterPunch > 0) {
        secondFighter.health -= firstFighterPunch;
      }
      if (secondFighterPunch > 0) {
        firstFighter.health -= secondFighterPunch;
      }
    }

    let msg;
    if (firstFighter.health < 0 && secondFighter.health < 0) {
      msg = 'Draw!';
    } else if (firstFighter.health < 0) {
      msg = `${secondFighter.name} wins!`;
    } else if (secondFighter.health < 0) {
      msg = `${firstFighter.name} wins!`;
    }

    this.modal.setContent(
      `<h2>${msg}</h2>
      <input id="_id1" type="hidden" value="${firstFighter._id}">
      <input id="_id2" type="hidden" value="${secondFighter._id}">`
    );
    this.modal.open();
  }
}

export default ControlsView;
