class Fighter {
  constructor({ _id, source, name, health, attack, defense }) {
    this._id = _id;
    this.source = source;
    this.name = name;
    this.health = health;
    this.attack = attack;
    this.defense = defense;
  }

  getHitPower() {
    return this.attack * (Math.random() + 1);
  }

  getBlockPower() {
    return this.defense * (Math.random() + 1);
  }
}

export default Fighter;
